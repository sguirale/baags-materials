#!/bin/bash

#SBATCH -p general
#SBATCH -N 1
#SBATCH --job-name=cpgenome_assembly
#SBATCH --mem 50G
#SBATCH --cpus-per-task=10
#SBATCH -t 11-0
#SBATCH --out=cpgenome_assembly


# add Modules
module add anaconda

# activate anaconda environment
source activate grasstool

# change directory.
cd /pine/scr/w/z/wzhou10/PASTOS23/cpgenome/filter_illumina/

# step1 of getorganelle download all datasets
get_organelle_config.py -a all &&

# step2 of getorganelle to generate the cpgenome.
get_organelle_from_reads.py -1 /pine/scr/w/z/wzhou10/PASTOS23/cpgenome/filter_illumina/P23_filter_R1.fastq -2 /pine/scr/w/z/wzhou10/PASTOS23/cpgenome/filter_illumina/P23_filter_R2.fastq -o P23_cp -R 15 -k 21,39,45,65,85,105 -F embplant_pt -t 10

# use flye for long filtered long reads data.
flye --nano-raw filter_new_ONT_1k.fastq --genome-size 0.16m --out-dir ./flye_ONT_raw_output --threads 10
